# Fcebook parser for signed_request



		{
		  "thread_type": "GROUP",
		  "tid": "1411911565550430",
		  "psid": "1293479104029354",
		  "signed_request": "5f8i9XXH2hEaykXHKFvu-E5Nr6QRqN002JO7yl-w_9o.eyJhbGdvcml0aG0iOiJITUFDLVNIQTI1NiIsImlzc3VlZF9hdCI6MTUwNDA0NjM4MCwicGFnZV9pZCI6NjgyNDk4MTcxOTQzMTY1LCJwc2lkIjoiMTI1NDQ1OTE1NDY4MjkxOSIsInRocmVhZF90eXBlIjoiVVNFUl9UT19QQUdFIiwidGlkIjoiMTI1NDQ1OTE1NDY4MjkxOSJ9"
		}
	  

#Usage

	fb_parse_signed_request("signed_request","facebook_app_secret);
	
Return `Error Object` or `data` like: 
	
	
	{ 
		  algorithm: 'HMAC-SHA256',
		  issued_at: 1504046380,
		  page_id: 682498171943165,
		  psid: '1254459154682919',
		  thread_type: 'USER_TO_PAGE',
		  tid: '1254459154682919' 
	  }


#Tests

	npm test
