"use srict";

/**
 * @author Jay
 */
const crypto = require('crypto'),
    assert = require('chai').assert,
    expect = require('chai').expect,
    parse_signed_request = require("./");

let _secret = "mytestkey"
const hmac = crypto.createHmac('sha256', _secret);
let request = Buffer.from(JSON.stringify({
    algorithm: 'HMAC-SHA256'
})).toString('base64');
let signed_request, singature, signed_request_result;

before((done) => {
    singature = hmac.update(request).digest('base64').replace(/[^\w\s]/gi, "").replace("_", "");
    // Generate custom encoded example
    signed_request = `${singature}.${request}`;
    signed_request_result = parse_signed_request(signed_request, _secret);
    done();
});

describe('Test parse_signed_request ', () => {
    it('Test prerequisites', async () => {
        expect(singature).to.equal("lLGiByyUpCy7l2mMlNHEFnvdCoDqsQ579EDerZtu4H4");
        expect(signed_request).to.equal("lLGiByyUpCy7l2mMlNHEFnvdCoDqsQ579EDerZtu4H4.eyJhbGdvcml0aG0iOiJITUFDLVNIQTI1NiJ9");
    });
    it('Test signed request', async () => {
        expect(signed_request_result).to.be.an('object');
        expect(signed_request_result.algorithm).to.equal("HMAC-SHA256");
    });
});
