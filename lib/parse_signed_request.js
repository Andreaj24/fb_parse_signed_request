"use strict";
const crypto = require('crypto');
/**
 *
 * @param   {String} _signed_request  Your Facebook signed_request param
 * @param   {String} _secret          Your Facebook Secret app
 * @return  {Object}                  The Data in Payload
 * @return  {Error}                   The Error Object
 */
exports.parse = function (_signed_request, _secret) {
    const hmac = crypto.createHmac('sha256', _secret);
    let signed_request = _signed_request.split('.'),
        encoded_sig = signed_request[0].replace(/[^\w\s]/gi, "").replace("_", ""),
        payload = signed_request[1];
    let data = JSON.parse(Buffer.from(payload, 'base64').toString());
    if (data.algorithm.toUpperCase() !== 'HMAC-SHA256') return new Error("Algorithm error, expected HMAC-SHA256");
    let sig = hmac.update(payload).digest('base64').replace(/[^\w\s]/gi, "").replace("_", "");
    if (encoded_sig !== sig) return new Error("Signature Failure");
    return data;
};
